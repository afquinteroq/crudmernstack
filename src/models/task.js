const mongoose = require('mongoose');
const { Schema } = mongoose;

const TaskSchema = new Schema({
  title: { type: String, required: true },
  claves: { type: String, required: true }, 
  description: { type: String, required: true },    
  fuente: { type: String, required: true },
  tiporecurso: { type: String, required: true },
  cobertura: { type: String, required: true },
});

module.exports = mongoose.model('CRUD', TaskSchema);
