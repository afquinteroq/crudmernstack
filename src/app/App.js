import React, { Component } from 'react';

class App extends Component {

  constructor() {
    super();
    this.state = {
      title: '',
      claves: '',
      description: '',
      fuente: '',
      tiporecurso: '',
      cobertura: '',
      _id: '',
      tasks: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.addTask = this.addTask.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }

  addTask(e) {
    e.preventDefault();
    if(this.state._id) {
      fetch(`/api/tasks/${this.state._id}`, {
        method: 'PUT',
        body: JSON.stringify({
          title: this.state.title,
          claves: this.state.claves,
          description: this.state.description,
          fuente: this.state.fuente,
          tiporecurso: this.state.tiporecurso,
          cobertura: this.state.cobertura
        }),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          window.M.toast({html: 'Registro actualizado'});
          this.setState({_id: '', title: '', claves: '', description: '', fuente: '', tiporecurso: '', cobertura: ''});
          this.fetchTasks();
        });
    } else {
      fetch('/api/tasks', {
        method: 'POST',
        body: JSON.stringify(this.state),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          window.M.toast({html: 'Registro guardado'});
          this.setState({title: '', claves: '', description: '', fuente: '', tiporecurso: '', cobertura: ''});
          this.fetchTasks();
        })
        .catch(err => console.error(err));
    }

  }

  deleteTask(id) {
    if(confirm('Seguro de borrar el registro?')) {
      fetch(`/api/tasks/${id}`, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          M.toast({html: 'Registro borrado'});
          this.fetchTasks();
        });
    }
  }

  editTask(id) {
    fetch(`/api/tasks/${id}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        this.setState({
          title: data.title,
          claves: data.claves,
          description: data.description,
          fuente: data.fuente,
          tiporecurso: data.tiporecurso,
          cobertura: data.cobertura,
          _id: data._id
        });
      });
  }

  componentDidMount() {
    this.fetchTasks();
  }

  fetchTasks() {
    fetch('/api/tasks')
      .then(res => res.json())
      .then(data => {
        this.setState({tasks: data});
        console.log(this.state.tasks);
      });
  }

  render() {
    return (
      <div>
        {/* NAVIGATION */}
        <nav id="navid" className="light-green darken-4">
          <div className="container">
            <div className="nav-wrapper">
              Prueba Ingeniero de Desarrollo de Software Comisión de la Verdad              
            </div>
          </div>
        </nav>

        <div className="container">
          <div className="row">
            <div className="col s5">
              <div className="card">
                <div className="card-content">
                  <form onSubmit={this.addTask}>
                    <div className="row">
                      <div className="input-field col s12">
                        <input name="title" onChange={this.handleChange} value={this.state.title} type="text" placeholder="Titulo" autoFocus/>
                      </div>
                    </div>
                    <div className="row">
                      <div className="input-field col s12">
                        <textarea name="claves" onChange={this.handleChange} value={this.state.claves} cols="30" rows="10" placeholder="Claves" className="materialize-textarea"></textarea>
                      </div>
                    </div>
                    <div className="row">
                      <div className="input-field col s12">
                        <textarea name="description" onChange={this.handleChange} value={this.state.description} cols="30" rows="10" placeholder="Descripción" className="materialize-textarea"></textarea>
                      </div>
                    </div>
                    <div className="row">
                      <div className="input-field col s12">
                        <textarea name="fuente" onChange={this.handleChange} value={this.state.fuente} cols="30" rows="10" placeholder="Fuente" className="materialize-textarea"></textarea>
                      </div>
                    </div>
                    <div className="row">
                      <div className="input-field col s12">
                        <textarea name="tiporecurso" onChange={this.handleChange} value={this.state.tiporecurso} cols="30" rows="10" placeholder="Tipo Recurso" className="materialize-textarea"></textarea>
                      </div>
                    </div>
                    <div className="row">
                      <div className="input-field col s12">
                        <textarea name="cobertura" onChange={this.handleChange} value={this.state.cobertura} cols="30" rows="10" placeholder="Cobertura" className="materialize-textarea"></textarea>
                      </div>
                    </div>

                    <button type="submit"  className="btn light-green darken-4">
                      Enviar
                    </button>
                  </form>
                </div>
              </div>
            </div>
            <div className="col s7">
              <table className="responsive-table">
                <thead>
                  <tr>
                    <th>TITULO</th>
                    <th>CLAVE</th>
                    <th>DESCRIPCIÓN</th>
                    <th>FUENTE</th>
                    <th>TIPO RECURSO</th>
                    <th>COBERTURA</th>
                  </tr>
                </thead>
                <tbody>
                  { 
                    this.state.tasks.map(task => {
                      return (
                        <tr key={task._id}>
                          <td>{task.title}</td>
                          <td>{task.claves}</td>
                          <td>{task.description}</td>
                          <td>{task.fuente}</td>
                          <td>{task.tiporecurso}</td>
                          <td>{task.cobertura}</td>
                          <td>
                            <button onClick={() => this.deleteTask(task._id)} className="btn light-green darken-4">
                              <i className="material-icons">delete</i> 
                            </button>
                            <button onClick={() => this.editTask(task._id)} className="btn light-green darken-4" style={{margin: '4px'}}>
                              <i className="material-icons">edit</i>
                            </button>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    )
  }
}

export default App;
