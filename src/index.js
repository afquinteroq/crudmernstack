const express = require('express');
const morgan = require('morgan');
const path = require('path');

const app = express();

//conexión a base de datos
const { mongoose } = require('./database');

//configuración
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Rutas
app.use('/api/tasks', require('./routes/crud.routes'));

//archivos staticos
app.use(express.static(path.join(__dirname, 'public')));;

//incializar el servidor
app.listen(app.get('port'), () => {
  console.log(`Servidor en puerto ${app.get('port')}`);
});
