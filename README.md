# MERN STACK
esta es un ejemplo de aplicación CRUD desarrollada con Read, Node, Express y MongoDB
1. IDE de desarollo Visual Estudio Code version 1.39.1
2. se conecta a una base de datos MongoDB en la nube, plataforma MongoDB ATLAS
3. Se puede editar el archivo database.js  para conectarse a una base de datos local
4. Instalación de dependencias
    4.1 npm install morgan
    4.2 npm install express
    4.3 npm install mongoose
    4.4 npm install babel-core -D
    4.5 npm install babel-loader -D
    4.6 npm install babel-preset-env -D
    4.7 npm install babel-preset-react -D
    4.8 npm install nodemon -D
    4.9 npm install react -D
    4.10 npm install react-dom -D
    4.11 npm installwebpack -D
    4.12 npm install webpack-cli -D
5. 

